import Botao from './Botao';
import SuperBotao from './SuperBotao';

window.onload =  () => {
    var bt = new Botao();
    bt.addText("clique aqui");

    var superbt = new SuperBotao();
    superbt.addText("super botão");
    superbt.addEvents();

    document.body.appendChild(bt.view);
    document.body.appendChild(superbt.view);
}
